import mediaplayerController from './js/mediaplayerController.js';
import init from './js/scene.js';

window.onload = function () {
  const canvas = document.getElementById( 'canvas' );

  const width = canvas.clientWidth;
  const height = canvas.clientHeight;
  const pixelRatio = window.devicePixelRatio;

  init( canvas, width, height, pixelRatio, './' );
  mediaplayerController();
};
