import * as THREE from '/build/three.module.js';

import { OrbitControls } from '/jsm/controls/OrbitControls.js';
import { GLTFLoader } from '../lib/jsm/loaders/GLTFLoader.js';
import { TWEEN } from '/jsm/libs/tween.module.min.js';


var camera, renderer, controls, mixer, clock, scene, crowndInstancedMesh, dummy;

var animations = [];

var crowdMovement = [];

var spotLight1, spotLight2, drone1, drone2, laserBeam1, laserBeam2;

let crowd = 400;

function init( canvas, width, height, pixelRatio, path ) {

	clock = new THREE.Clock();
	scene = new THREE.Scene();
	dummy = new THREE.Object3D();

	//----------------------------------- CAMERA -----------------------------------

	camera = new THREE.PerspectiveCamera( 70, width / height, 100, 120000 );
	camera.position.z = 2000;
	scene.add(camera);

	//var cameraLight = new THREE.PointLight( new THREE.Color( 1, 1, 1 ), 0.5 );
	//cameraLight.castShadow = true;
	//camera.add( cameraLight );

	//----------------------------------- AMBIENT LIGHT -----------------------------------

	const light = new THREE.AmbientLight( 0x404040, 1 );
	light.castShadow = true;
	scene.add( light );

	//----------------------------------- SPOTLIGHTS -----------------------------------

	spotLight1 = createSpotlight( 0x99ffe6 );
	spotLight1.castShadow = true;
	scene.add( spotLight1 );

	spotLight2 = createSpotlight( 0x99ffe6 );
	spotLight2.castShadow = true;
	scene.add(spotLight2);

	//----------------------------------- SKYBOX -----------------------------------

	let materialArray = [];
	let texture_ft = new THREE.TextureLoader().load( '../assets/textures/kurt/space_ft.png');
	let texture_bk = new THREE.TextureLoader().load( '../assets/textures/kurt/space_bk.png');
	let texture_up = new THREE.TextureLoader().load( '../assets/textures/kurt/space_up.png');
	let texture_dn = new THREE.TextureLoader().load( '../assets/textures/kurt/space_dn.png');
	let texture_rt = new THREE.TextureLoader().load( '../assets/textures/kurt/space_rt.png');
	let texture_lf = new THREE.TextureLoader().load( '../assets/textures/kurt/space_lf.png');
	materialArray.push(new THREE.MeshBasicMaterial( { map: texture_ft }));
	materialArray.push(new THREE.MeshBasicMaterial( { map: texture_bk }));
	materialArray.push(new THREE.MeshBasicMaterial( { map: texture_up }));
	materialArray.push(new THREE.MeshBasicMaterial( { map: texture_dn }));
	materialArray.push(new THREE.MeshBasicMaterial( { map: texture_rt }));
	materialArray.push(new THREE.MeshBasicMaterial( { map: texture_lf }));

	for (let i = 0; i < 6; i++)
		materialArray[i].side = THREE.BackSide;
	let skyboxGeo = new THREE.BoxGeometry( 120000, 120000, 120000 );
	let skybox = new THREE.Mesh( skyboxGeo, materialArray );
	skybox.position.y += 19400;
	scene.add( skybox );

	//----------------------------------- GROUND -----------------------------------

	const textureLoader = new THREE.TextureLoader();
	const groundTexture = textureLoader.load( '../assets/textures/grass.png' );
	groundTexture.wrapS = groundTexture.wrapT = THREE.RepeatWrapping;
	groundTexture.repeat.set( 60, 60 );
	groundTexture.anisotropy = 16;
	groundTexture.encoding = THREE.sRGBEncoding;
	const groundMaterial = new THREE.MeshPhongMaterial( { map: groundTexture } );
	groundMaterial.emissiveIntensity = 0.1;
	let ground = new THREE.Mesh( new THREE.PlaneGeometry( 120000, 120000 ), groundMaterial );
	ground.position.y = - 470;
	ground.rotation.x = - Math.PI / 2;
	ground.receiveShadow = true;
	scene.add( ground );


	//----------------------------------- STAGE -----------------------------------


	load3DModel ('../assets/model/stage/scene.gltf', new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 4, 4, 4 ), false, function ( model ) {
		scene.add(model);
	});

	//----------------------------------- BAND -----------------------------------

	load3DModel ('../assets/model/band/scene.gltf', new THREE.Vector3( 0, 0, 3000 ), new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 9, 9, 9 ), true, function ( model ) {
		scene.add(model);
	});

	//----------------------------------- AMPLIFIRE -----------------------------------


	load3DModel ('../assets/model/orange_amp/scene.gltf', new THREE.Vector3( -1200, 0, 2500 ), new THREE.Vector3( 0, Math.PI / 6, 0 ), new THREE.Vector3( 20, 20, 20 ), false, function ( model ) {
		scene.add(model);
	});

	//----------------------------------- GUITAR SET -----------------------------------


	load3DModel ('../assets/model/telecaster_guitar_set/scene.gltf', new THREE.Vector3( 1000, 0, 2500 ), new THREE.Vector3( 0, - Math.PI / 6, 0 ), new THREE.Vector3( 25, 25, 25 ), false, function ( model ) {
		scene.add(model);
	});

	//----------------------------------- SPEAKERS -----------------------------------

	load3DModel ('../assets/model/speaker/scene.gltf', new THREE.Vector3( 3200, 0, 3000 ), new THREE.Vector3( 0, - Math.PI / 4, 0 ), new THREE.Vector3( 30, 30, 30 ), false, function ( model ) {
		scene.add(model);
		var secondSpeaker = model.clone();
		secondSpeaker.position.set( -3200, 0, 3000 );
		secondSpeaker.rotation.set( 0, 0, 0 );
		scene.add(secondSpeaker);
	});

	//----------------------------------- DRONES -----------------------------------

	load3DModel ('../assets/model/drone/scene.gltf', new THREE.Vector3( 15, 1400, 3000 ), new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 150, 150, 150 ), false, function ( model ) {
		drone1 = model;
		scene.add(drone1);
		drone2 = model.clone();
		scene.add(drone2);
		light_animation();
	});

	//----------------------------------- MOUNTAINS -----------------------------------

	load3DModel ('../assets/model/mountains/scene.gltf', new THREE.Vector3( 500, 9000, -70000 ), new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 800, 800, 800 ), false, function ( model ) {
		scene.add(model);
	});

	//----------------------------------- BANANA CROWD -----------------------------------

	crowd_random_movement();
	load3DModel ('../assets/model/banana/scene.gltf', new THREE.Vector3( 0, 0, 5000 ), new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 2, 2, 2 ), false, function ( model ) {
		model.traverse( function ( child ) {
			if ( child.isMesh ) {
				child.castShadow = true;
				child.receiveShadow = true;
				child.material.emissiveIntensity = 0.8;
				//child.material.metalness = 0;
				var prevMaterial = child.material;
				child.material = new THREE.MeshPhongMaterial();
				THREE.MeshBasicMaterial.prototype.copy.call( child.material, prevMaterial );
				crowndInstancedMesh = new THREE.InstancedMesh( child.geometry, child.material, crowd );
				dummy.scale.set( 2, 2, 2);
				dummy.updateMatrix();
				//crowndInstancedMesh.instanceMatrix.setUsage( THREE.DynamicDrawUsage );
				scene.add( crowndInstancedMesh );
			}
		});
	});

	//----------------------------------- LASERS -----------------------------------

	var laser_geometry = new THREE.CylinderGeometry( 50, 1, 50000, 32 );

	var shaderProp1 = {
		vertexShader: document.getElementById( 'vertexShaderNormal' ).textContent,
		fragmentShader: document.getElementById( 'fragmentShaderNormal' ).textContent
	};
	var shaderProp2 = {
		vertexShader: document.getElementById( 'vertexShaderNormal' ).textContent,
		fragmentShader: document.getElementById( 'FragmentShaderDiffuseLight' ).textContent
	};

	var shaderProp3 = {
		vertexShader: document.getElementById( 'vertexShaderHalfWayVect' ).textContent,
		fragmentShader: document.getElementById( 'FragmentShaderSpecularLight' ).textContent
	};

	var laserMat = new THREE.ShaderMaterial( shaderProp1 );
	laserBeam1 = new THREE.Mesh( laser_geometry, laserMat );
	laserBeam1.position.set( 3500, 0, 3500 );
	scene.add( laserBeam1 );

	laserBeam2 = new THREE.Mesh( laser_geometry, laserMat );
	laserBeam2.position.set( -3500, 0, 3500 );
	scene.add( laserBeam2 );


	renderer = new THREE.WebGLRenderer( { antialias: true, canvas: canvas } );
	renderer.setPixelRatio( pixelRatio );
	renderer.setSize( width, height, false );
	//renderer.outputEncoding = THREE.sRGBEncoding;
	renderer.gammaOutput = true;
	renderer.gammaFactor = 2.2;

	controls = new OrbitControls( camera, renderer.domElement );
	controls.minDistance = 5550;
	controls.maxDistance = 15000;
	controls.autoRotate = true;
	controls.autoRotateSpeed = 1.0;
	controls.minPolarAngle = Math.PI/8;
	controls.maxPolarAngle = Math.PI/2;
	controls.update();

	animate();


}

function animate( ) {

	//----------------------------------- MODEL ANIMATIONS -----------------------------------

	if ( self.requestAnimationFrame ) {
		self.requestAnimationFrame( animate );
		var delta = clock.getDelta( );
		for ( let i = 0; i < animations.length; i ++ ) {
			animations[ i ].update( delta );
		}
	}

	//----------------------------------- CROWD ANIMATION -----------------------------------

	if(crowndInstancedMesh) {
		var position = new THREE.Vector3();

		var counter = 0;
		for ( var j = 0; j < crowd / 20; j++ ) {
			for ( var i = 0; i < 20; i++ ) {
				var currentMatrix = crowndInstancedMesh.getMatrixAt( counter, dummy.matrix );
				//dummy.position.set( 3800 - 400 * i + Math.random() * 100, -450, 5000 + 300 * j - Math.random() * 100  );
				//dummy.rotation.set( 0, Math.PI + Math.random() * 1.5 - 0.75, 0 );
				dummy.position.set( 3800 - 400 * i +  Math.sin(Date.now() * 0.00005 * crowdMovement[j * 20 + i].x) * 100, -450, 5000 + 300 * j  +  Math.sin(Date.now() * 0.00005 * crowdMovement[j * 20 + i].z) * 100);

				dummy.rotation.set( 0, Math.sin(Date.now() * 0.01 + (i + j) * Math.PI/4) + Math.PI, 0 );
				dummy.updateMatrix();
				crowndInstancedMesh.setMatrixAt( counter, dummy.matrix );
				counter++;
			}
		}
		crowndInstancedMesh.instanceMatrix.needsUpdate = true;
	}
	TWEEN.update();

	//----------------------------------- LASERS ANIMATION -----------------------------------

	laserBeam1.rotation.z = Math.sin(Date.now() * 0.001) * Math.PI/2 * 0.5;
	laserBeam2.rotation.z = - Math.sin(Date.now() * 0.001) * Math.PI/2 * 0.5;
	renderer.render( scene, camera );

}

var onWindowResized = function () {
	const canvas = document.getElementById( 'canvas' );
	const height = canvas.clientHeight;
	var width = window.innerWidth;
	renderer.setSize( width, height );
	camera.aspect = width / height;
	camera.updateProjectionMatrix();
	renderer.render( scene, camera );
};

function load3DModel ( source, position, rotation, scale, animated, callback ) {
	var loader = new GLTFLoader();
	loader.load( source, function ( gltf ) {
		var model = gltf.scene;
		const box = new THREE.Box3().setFromObject( model );
		const center = box.getCenter( new THREE.Vector3() );
		model.position.set(position.x, position.y, position.z);
		model.rotation.set(rotation.x, rotation.y, rotation.z);
		model.scale.set(scale.x, scale.y, scale.z);
		model.castShadow = true;
		model.receiveShadow = true;
		if (animated) {
			mixer = new THREE.AnimationMixer( model );
			gltf.animations.forEach( ( clip ) => {
				mixer.clipAction( clip ).play();
			} );
			animations.push(mixer);
		}
		if(callback)
		callback(model);
	}, undefined, function ( error ) {
		console.error( error );
	});
}

function createSpotlight( color ) {
	var newObj = new THREE.SpotLight( color, 1 );

	newObj.castShadow = true;
	newObj.penumbra = 0.2;

	newObj.power = 12 * Math.PI;

	const targetObject = new THREE.Object3D();
	scene.add(targetObject);

	newObj.target = targetObject;

	return newObj;
}

function tween( light, obj, isCrowd) {

	var speed = Math.random() * 3000 + 2000;
	new TWEEN.Tween( light ).to( {
		angle: ( Math.random() * 0.3 ) + 0.3,
		penumbra: Math.random() + 0.6
	}, speed )
	.easing( TWEEN.Easing.Quadratic.Out).start();

	let randomX, randomY, randomZ;
	if(isCrowd) {
		randomX = (Math.random() * 7000) - 3500;
		randomY = (Math.random() * 1000) + 1000;
		randomZ = ( Math.random() * 4000 ) + 6000;
	} else {
		randomX = (Math.random() * 2000) - 1000;
		randomY = (Math.random() * 1000) + 1000;
		randomZ = ( Math.random() * 500 ) + 2500;
	}

	new TWEEN.Tween( light.position ).to( {
		x: randomX,
		y: randomY,
		z: randomZ,
	}, speed )
	.easing( TWEEN.Easing.Quadratic.Out ).start();

	new TWEEN.Tween( light.target.position ).to( {
		x: randomX,
		y: 0,
		z: randomZ,
	}, speed )
	.easing( TWEEN.Easing.Quadratic.Out ).start();

	if (obj) {
		new TWEEN.Tween( obj.position ).to( {
			x: randomX,
			y: randomY,
			z: randomZ,
		}, speed )
		.easing( TWEEN.Easing.Quadratic.Out ).start();
	}
}

function light_animation() {
	tween( spotLight1, drone1, true);
	tween( spotLight2, drone2, false);
	setTimeout( light_animation, 5000 );
}

function crowd_random_movement() {

	for (var i = 0; i < crowd; i++) {
		var move = {x: (Math.random() * 200 - 100), z: (Math.random() * 200 - 100)}
		crowdMovement.push(move)
	}
}

window.addEventListener( 'resize', onWindowResized ) ;

export default init;
