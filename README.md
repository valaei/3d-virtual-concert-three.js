# Virtual Concert - A Great Gig in the Sky
UTS - Computer Graphic subject - 2021

Inspired by Pink Floyd!

Instructions:

npm install

npm start

-> http://localhost:3000

![A Great Gig in the Sky](./images/image-1.png "A Great Gig in the Sky")
![A Great Gig in the Sky](./images/image-2.png "A Great Gig in the Sky")

